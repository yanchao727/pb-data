#!/usr/bin/env python
import urllib2
import time
import csv
import os.path
from BeautifulSoup import BeautifulSoup

def downloader():
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    site = "https://thepiratebay.se/top/601"
    req = urllib2.Request(site, headers=hdr)
    try:
        page = urllib2.urlopen(req)
    except urllib2.HTTPError, e:
        str_error = "PARSE-ERROR\n " +  str(e.fp.read())
        return None 
    return page.read()
    
def parser(content):
    out_log = []
    cur_date = time.strftime("%m/%d/%Y")
    soup = BeautifulSoup(content)
    for count, row in enumerate(soup.findAll('a', attrs={'class':'detLink'})):
        temp = "%s; %s; %s\n" % (count,  row["title"][12:], cur_date)
        out_log.append(temp)
    return out_log

def log_error():
    error_file = time.strftime("%m%d%Y") + ".ERROR" + ".txt"
    f = open(error_file, "wb")
    f.close()

def run():
    date_file = time.strftime("%m%d%Y") + ".txt"
    content = downloader()
    top_100 = []
    if content  is not None:
        top_100 = parser(content)
        if len(top_100) is 100:
            working_path = os.path.join( r"/home/rol/projects/pb-scrap/data", date_file)
            with open(working_path,'wb') as out_file:
                for item in top_100:
                    out_file.write(item)
        else:
            log_error()
    else:
        log_error()
    
run()
